import zipfile
import shutil
import tempfile
import requests

from os import path
from dataclasses import dataclass

@dataclass
class Plugin:
    url: str
    branch: str = 'master'

#--- Globals ----------------------------------------------
PLUGINS = {
    'ag.vim'                     : Plugin('https://github.com/rking/ag.vim'),
    'ale'                        : Plugin('https://github.com/w0rp/ale'),
    'bufexplorer'                : Plugin('https://github.com/jlanzarotta/bufexplorer'),
    'coc.nvim'                   : Plugin('https://github.com/neoclide/coc.nvim', 'release'),
    'fzf.vim'                    : Plugin('https://github.com/junegunn/fzf.vim'),
    'nerdtree'                   : Plugin('https://github.com/scrooloose/nerdtree'),
    'nginx.vim'                  : Plugin('https://github.com/vim-scripts/nginx.vim'),
    'open_file_under_cursor.vim' : Plugin('https://github.com/amix/open_file_under_cursor.vim'),
    'peaksea'                    : Plugin('https://github.com/vim-scripts/peaksea'),
    'vim-airline'                : Plugin('https://github.com/vim-airline/vim-airline'),
    'vim-airline-themes'         : Plugin('https://github.com/vim-airline/vim-airline-themes'),
    'vim-colors-solarized'       : Plugin('https://github.com/altercation/vim-colors-solarized'),
    'vim-commentary'             : Plugin('https://github.com/tpope/vim-commentary'),
    'vim-dispatch'               : Plugin('https://github.com/tpope/vim-dispatch'),
    'vim-expand-region'          : Plugin('https://github.com/terryma/vim-expand-region'),
    'vim-fugitive'               : Plugin('https://github.com/tpope/vim-fugitive'),
    'vim-indent-object'          : Plugin('https://github.com/michaeljsmith/vim-indent-object'),
    'vim-markdown'               : Plugin('https://github.com/tpope/vim-markdown'),
    'vim-multiple-cursors'       : Plugin('https://github.com/terryma/vim-multiple-cursors'),
    'vim-repeat'                 : Plugin('https://github.com/tpope/vim-repeat'),
    'vim-surround'               : Plugin('https://github.com/tpope/vim-surround'),
}

SOURCE_DIR = path.join(path.dirname(__file__), 'sources_non_forked')

def download_extract_replace(name, plug : Plugin, temp_dir):
    temp_zip_path = path.join(temp_dir, name)

    # Download and extract file in temp dir
    zip_path = f'{plug.url}/archive/{plug.branch}.zip'
    req = requests.get(zip_path)
    open(temp_zip_path, 'wb').write(req.content)

    zip_f = zipfile.ZipFile(temp_zip_path)
    zip_f.extractall(temp_dir)

    plugin_temp_path = path.join(temp_dir, path.join(temp_dir, f'{name}-{plug.branch}'))

    # Remove the current plugin and replace it with the extracted
    plugin_dest_path = path.join(SOURCE_DIR, name)

    try:
        shutil.rmtree(plugin_dest_path)
    except OSError:
        pass

    shutil.move(plugin_temp_path, plugin_dest_path)

    print('Updated {0}'.format(name))


if __name__ == '__main__':
    temp_directory = tempfile.mkdtemp()

    try:
        for name, plug in PLUGINS.items():
            download_extract_replace(name, plug, temp_directory)
    finally:
        shutil.rmtree(temp_directory)
